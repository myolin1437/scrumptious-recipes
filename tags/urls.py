from django.urls import path

# from tags.views import show_tags

from tags.views import (
    TagListView,
)

# urlpatterns = [
#     path("", show_tags, name="tags_list"),
# ]

urlpatterns = [path("", TagListView.as_view(), name="tags_list")]
