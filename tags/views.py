from django.shortcuts import render
from django.views.generic.list import ListView

try:
    from tags.models import Tag
except Exception:
    Tag = None


# Create your views here.
def show_tags(request):
    context = {
        "tags": Tag.objects.all() if Tag else None,
    }
    return render(request, "tags/list.html", context)


class TagListView(ListView):
    model = Tag
    context_object_name = "tags"
    template_name = "tags/list.html"
