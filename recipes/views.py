from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from recipes.forms import RatingForm

try:
    from recipes.forms import RecipeForm
    from recipes.models import Recipe
except Exception:
    RecipeForm = None
    Recipe = None


# CREATE RECIPE VIEW
def create_recipe(request):
    if request.method == "POST" and RecipeForm:
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save()
            return redirect("recipe_detail", pk=recipe.pk)
    elif RecipeForm:
        form = RecipeForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/new.html", context)


class RecipeCreateView(CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = [
        "name",
        "author",
        "description",
        "image",
    ]
    success_url = reverse_lazy("recipes_list")


# EDIT RECIPE VIEW
def change_recipe(request, pk):
    if Recipe and RecipeForm:
        instance = Recipe.objects.get(pk=pk)
        if request.method == "POST":
            form = RecipeForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("recipe_detail", pk=pk)
        else:
            form = RecipeForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/edit.html", context)


class RecipeUpdateView(UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = [
        "name",
        "author",
        "description",
        "image",
    ]

    def get_success_url(self):
        return reverse_lazy("recipe_detail", kwargs={"pk": self.object.pk})


# RECIPE DETAIL VIEW
def show_recipe(request, pk):
    context = {
        "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
        "rating_form": RatingForm(),
    }
    return render(request, "recipes/detail.html", context)


class RecipeDetailView(DetailView):
    model = Recipe
    # context_object_name = "recipes"
    template_name = "recipes/detail.html"

    # By default, the DetailView class finds the object that we want it to find, then puts that in the context with the key "lower case «model name»". In our case, that means that it gets the Recipe for us and puts it into the context with the key "recipe".
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context


# RECIPES LIST VIEW
def show_recipes(request):
    context = {
        "recipes": Recipe.objects.all() if Recipe else [],
    }
    return render(request, "recipes/list.html", context)


class RecipeListView(ListView):
    model = Recipe
    context_object_name = "recipes"
    # use context_object_name to set a specific name for each instance of this class, otherwise, the object's name will be default to <model name>_list, all in lower case.
    # syntax: context_object_name: Optional[str] | default=<model name>_list
    # for example, if no context_object_name is used, the for loop in the list.html template will be "{% for recipe in recipe_list %}" instead
    template_name = "recipes/list.html"
    paginate_by = 3

    # TO ADD A SEARCH BAR BASED ON A SPECIFIC TAG
    def get_queryset(self):
        query_string = self.request.GET.get("q", "")
        return Recipe.objects.filter(description__icontains=query_string)


# RECIPE DELETE VIEW
class RecipeDeleteView(DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


# LOG RATING FUNCTION
def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)
