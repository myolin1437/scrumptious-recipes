from django.urls import path

# from recipes.views import (
#     create_recipe,
#     change_recipe,
#     log_rating,
#     show_recipe,
#     show_recipes,
# )

from recipes.views import (
    RecipeCreateView,
    RecipeDeleteView,
    RecipeUpdateView,
    log_rating,
    RecipeDetailView,
    RecipeListView,
)

# urlpatterns = [
#     path("", show_recipes, name="recipes_list"),
#     path("<int:pk>/", show_recipe, name="recipe_detail"),
#     path("new/", create_recipe, name="recipe_new"),
#     path("edit/", change_recipe, name="recipe_edit"),
#     path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
# ]

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
]
